#include "Restaurant.h"

Restaurant::Restaurant() :nom_(new string("inconnu")), chiffreAffaire_(0.0), momentJournee_(Matin), capaciteTables_(MAXCAP), nbTables_(0){
	menuMatin_ = new Menu();
	menuMidi_ = new Menu();
	menuSoir_ = new Menu();
	tables_ = new Table*[capaciteTables_]();



};

Restaurant::Restaurant(string& fichier, string& nom, TypeMenu moment) :nom_(new string(nom)), chiffreAffaire_(0.0), momentJournee_(moment), capaciteTables_(MAXCAP), nbTables_(0){
	menuMatin_ = new Menu(fichier, Matin);
	menuMidi_ = new Menu(fichier, Midi);
	menuSoir_ = new Menu(fichier, Soir);
	tables_ = new Table*[capaciteTables_]();
	lireTable(fichier);
};

void Restaurant::setMoment(TypeMenu moment) {
	momentJournee_ = moment;
};

string Restaurant::getNom() {
	return *nom_;
};

TypeMenu Restaurant::getMoment() {
	return momentJournee_;
};

void Restaurant::lireTable(string& fichier) {
	ifstream entree(fichier);
	string typeVoulu = "-TABLES", typeLu = " ";
	int id = 0, nbPlaces = 0;
	while (!ws(entree).eof()) {
		entree >> typeLu;
		if (typeVoulu == typeLu) {
			while (!ws(entree).eof()) {
				entree >> id;
				entree >> nbPlaces;
				ajouterTable(id, nbPlaces);
			}
		}
		else
			entree.ignore(80, '\n');
	}
};


void Restaurant::ajouterTable(int id, int nbPlaces) {
	Table* table = new Table(id, nbPlaces);
	tables_[nbTables_++] = table;


};



void Restaurant::libererTable(int id) {
	chiffreAffaire_ += tables_[id - 1]->getChiffreAffaire();
	tables_[id - 1]->libererTable();

};

void Restaurant::commanderPlat(string& nom, int idTable) {
	Plat* ptr = new Plat();
	switch (momentJournee_) {
	case 0: ptr = menuMatin_->trouverPlat(nom); break;
	case 1: ptr = menuMidi_->trouverPlat(nom); break;
	case 2: ptr = menuSoir_->trouverPlat(nom); break;

	}

	tables_[idTable - 1]->commander(ptr);
};

void Restaurant::placerClients(int nbClients) {
	int nbTablesOccupee = 0, difference = 0, differenceMin = 6, indiceMin = 0;
	for (int i = 0; i < nbTables_; ++i) {
		if (tables_[i]->estOccupee())
			nbTablesOccupee++;
		else {
			difference = (tables_[i]->getNbPlaces()) - nbClients;
			if (difference <= differenceMin && difference >= 0) {
				indiceMin = i;
				differenceMin = difference;
			}
		}
	}
	if (nbTablesOccupee == nbTables_ || differenceMin == 6)
		cout << "Erreur : il n'y a plus de tables disponibles pour le groupe de " << nbClients << " personnes." << endl << endl; 

	else
		tables_[indiceMin]->placerClient();


};


void Restaurant::afficher() {
	cout << "Le restaurant PolyFood a fait un chiffre d'affaire de : " << chiffreAffaire_ << "$" << endl;
	cout << "-Voici les tables :" << endl;
	for (int i = 0; i < nbTables_; ++i)
		tables_[i]->afficher();

	cout << "-Voici son menu :" << endl;
	menuMatin_->afficher();
	menuMidi_->afficher();
	menuSoir_->afficher();


};
