#include "Table.h"

Table::Table() :capacite_(MAXCAP), nbPlats_(0), id_(-1), nbPlaces_(1), occupee_(false) {
	commande_ = (new Plat*[capacite_]());
};

Table::Table(int id, int nbPlaces) :capacite_(MAXCAP), nbPlats_(0), id_(id), nbPlaces_(nbPlaces), occupee_(false) {
	commande_ = (new Plat*[capacite_]());

};

int Table::getId() {
	return id_;
};

int Table::getNbPlaces() {
	return nbPlaces_;
};

bool Table::estOccupee() {
	return occupee_;
};

void Table::libererTable() {
	for (int i = 0; i < nbPlats_; i++)
		commande_[i] = nullptr;
	delete[] commande_;
	commande_ = nullptr;
	occupee_ = false;
};

void Table::placerClient() {
	occupee_ = true;


};

void Table::setId(int id) {
	id_ = id;
};

void Table::commander(Plat* plat) {
	commande_[nbPlats_] = plat;
	nbPlats_++;
	
};

double Table::getChiffreAffaire() {
	double chiffreAffaire = 0.0, gain = 0.0, perte = 0.0;
	for (int i = 0; i < nbPlats_; i++) {
		if (commande_[i] != nullptr) {
			gain += commande_[i]->getPrix();
			perte += commande_[i]->getCout();
		}
	}

	chiffreAffaire = gain - perte;
	return chiffreAffaire;
};

void Table::afficher() {

	if (occupee_) {
		if (nbPlats_ > 0) {
			cout << "      La table numero " << id_ << " est occupee. Voici la commande passee par les clients :" << endl;
		}
		else
			cout << "      La table numero " << id_ << " est occupee. Or, il n'y a pas de commande en ce moment." << endl;

		for (int i = 0; i < nbPlats_; i++) {
			if (commande_[i] != nullptr)
				commande_[i]->afficher();
			else
				cout << "      L'aliment demande n'a pas ete trouve." << endl;
		}
		cout << endl;
	}
	else
		cout << "      La table numero " << id_ << " est libre." << endl << endl;


};