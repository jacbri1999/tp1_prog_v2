#include "Menu.h"

Menu::Menu() :capacite_(MAXPLAT), nbPlats_(0), type_(Matin) {
	listePlats_ = new Plat*[capacite_]();
	//for (int i = 0; i < capacite_; i++)
	//	listePlats_[i] = nullptr;

};

Menu::Menu(string fichier, TypeMenu type) :capacite_(MAXPLAT), nbPlats_(0), type_(type) {
	listePlats_ = new Plat*[capacite_]();
	lireMenu(fichier);

};

int Menu::getNbPlats() {
	return nbPlats_;
};

void Menu::afficher() {
	switch (type_) {
	case 0: cout << "Matin :" << endl; break;
	case 1: cout << "Midi :" << endl; break;
	case 2: cout << "Soir :" << endl; break;
	}
	for (int i = 0; i < nbPlats_; i++)
		listePlats_[i]->afficher();
	cout << endl;
};

Plat* Menu::trouverPlat(string& nom) {
	string nomPlat = " ";
	for (int i = 0; i < nbPlats_; i++) {
		nomPlat = listePlats_[i]->getNom();
		if (nom == nomPlat)
			return listePlats_[i];
	}
	return nullptr;
};

void Menu::ajouterPlat(Plat & plat) {
	*listePlats_[nbPlats_] = plat;
	nbPlats_++;
};

void Menu::ajouterPlat(string& nom, double montant, double cout) {
	listePlats_[nbPlats_]->setNom(nom);
	listePlats_[nbPlats_]->setPrix(montant);
	listePlats_[nbPlats_]->setCout(cout);

	nbPlats_++;
};

bool Menu::lireMenu(string& fichier) {
	ifstream entree(fichier);
	string typeVoulu = " ", typeLu = " ", prochainType = " ", nomTemporaire = " ";
	int i = 0;
	double prix = 0.0, cout = 0.0;
	switch (type_) {
	case (Matin): typeVoulu = "-MATIN"; prochainType = "-MIDI"; break;
	case (Midi): typeVoulu = "-MIDI"; prochainType = "-SOIR"; break;
	case (Soir): typeVoulu = "-SOIR"; prochainType = "-TABLES"; break;
	}

	while (!ws(entree).eof()) {
		entree >> typeLu;
		if (typeLu == typeVoulu) {
			entree >> nomTemporaire;
			do {
				entree >> prix;
				entree >> cout;
				Plat* plat = new Plat(nomTemporaire, prix, cout);
				listePlats_[i] = plat;
				entree >> nomTemporaire;
				i++;
				nbPlats_++;
			} while (nomTemporaire != prochainType);

		}
		else
			entree.ignore(80, '\n');
	}
	return true;
};